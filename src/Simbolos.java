
public enum Simbolos {
	SETE("Sete", 300),
	BANANA("Banana", 10),
	FRAMBOESA("Framboesa", 50),
	MOEDA("Moeda", 100),
	MACA("Maça", 5);
	
	public String getNome() {
		return nome;
	}

	public int getValor() {
		return valor;
	}

	private String nome;
	private int valor;
	
	Simbolos(String nome, int valor) {
		this.nome = nome;
		this.valor = valor;
	}

}
