import java.util.List;
import java.util.ArrayList;

public class Maquina {
	public List<Slot> listaSlot = new ArrayList<Slot>();

	public int pontuaca = 0;
	
	public Maquina(int qtdslots) {
		for (int i = 0; i < qtdslots; i++) {
			listaSlot.add(new Slot());
		}
	}

	public void sortear() {

		for (Slot slot : listaSlot) {
			slot.girar();	
		}

	}

	public void calcularPontuacao() {
		this.pontuaca = 0;
		
		for (Slot slot : listaSlot) {
			this.pontuaca += slot.simbolos.getValor();
		}
		
		

		boolean slotsIguais = true;

		Simbolos anterior = null;
		Simbolos atual = null;
		
		
		

		for (int i = 0; i < listaSlot.size(); i++) {
			anterior = atual;
			atual = listaSlot.get(i).simbolos;
			if (i != 0 && slotsIguais) {
				if (anterior != atual) {
					slotsIguais = false;
					break;
				}
			}
		}

	//	this.pontuaca += atual.getValor();

		if (slotsIguais) {
			this.pontuaca *= 100;
		}

	}
	
	public String toString() {
		String model = "Total: " + this.pontuaca + "\n";
		
		for (Slot slot : listaSlot) {
			model += slot + "\n";
		}
		return model;
	}

	public boolean verificarIguais () {
		for (Slot slot : listaSlot) {
			if (!slot.simbolos.equals(listaSlot.get(0).simbolos));
			return false;
		}
		return true;
	}
	
}
